<?php 
    use App\Http\Controllers\Sentinel;
    $var = new Sentinel();
?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'citations', 'read')){ ?>
<li class="{{ Request::is('citations*') ? 'active' : '' }}">
    <a href="{!! route('citations.index') !!}"><i class="fa fa-edit"></i><span>Citations</span></a>
</li>
<?php } ?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'faq', 'read')){ ?>
<li class="{{ Request::is('faqs*') ? 'active' : '' }}">
    <a href="{!! route('faqs.index') !!}"><i class="fa fa-edit"></i><span>Faqs</span></a>
</li>
<?php } ?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'groups', 'read')){ ?>
<li class="{{ Request::is('groups*') ? 'active' : '' }}">
    <a href="{!! route('groups.index') !!}"><i class="fa fa-edit"></i><span>Groups</span></a>
</li>
<?php } ?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'events', 'read')){ ?>
<li class="{{ Request::is('events*') ? 'active' : '' }}">
    <a href="{!! route('events.index') !!}"><i class="fa fa-edit"></i><span>Events</span></a>
</li>
<?php } ?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'profiles', 'read')){ ?>
<li class="{{ Request::is('profiles*') ? 'active' : '' }}">
    <a href="{!! route('profiles.index') !!}"><i class="fa fa-edit"></i><span>Profiles</span></a>
</li>
<?php } ?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'pushes', 'read')){ ?>
<li class="{{ Request::is('pushes*') ? 'active' : '' }}">
    <a href="{!! route('pushes.index') !!}"><i class="fa fa-edit"></i><span>Notifications</span></a>
</li>
<?php } ?>


<?php if ( $var->checkPermission(\Auth::user()->id, 'roles', 'read')){ ?>
<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>{{trans('project.roles')}}</span></a>
</li>
<?php } ?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'actions', 'read')){ ?>
<li class="{{ Request::is('actions*') ? 'active' : '' }}">
    <a href="{!! route('actions.index') !!}"><i class="fa fa-edit"></i><span>{{trans('project.actions')}}</span></a>
</li>
<?php } ?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'modules', 'read')){ ?>
<li class="{{ Request::is('modules*') ? 'active' : '' }}">
    <a href="{!! route('modules.index') !!}"><i class="fa fa-edit"></i><span>{{trans('project.modules')}}</span></a>
</li>
<?php } ?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'role_user', 'read')){ ?>
<li class="{{ Request::is('roleUsers*') ? 'active' : '' }}">
    <a href="{!! route('roleUsers.index') !!}"><i class="fa fa-edit"></i><span>{{trans('project.role')}} {{trans('project.user')}}</span></a>
</li>
<?php } ?>

<?php if ( $var->checkPermission(\Auth::user()->id, 'module_action', 'read')){ ?>
<li class="{{ Request::is('moduleActions*') ? 'active' : '' }}">
    <a href="{!! route('moduleActions.index') !!}"><i class="fa fa-edit"></i><span>{{trans('project.module')}} {{trans('project.actions')}}</span></a>
</li>
<?php } ?><li class="{{ Request::is('documents*') ? 'active' : '' }}">
    <a href="{!! route('documents.index') !!}"><i class="fa fa-edit"></i><span>Documents</span></a>
</li>

