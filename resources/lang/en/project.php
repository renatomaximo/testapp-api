<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'TestApp',
    'urlDeveloper' => 'TestApp',
    'nameDeveloper' => 'TestApp',
    'sistemaVersao' => 'v0.1',
    'textDeveloper' => 'Developed by',
    'view_more' => 'View More',
    'add_new' => 'Add New',
    'events' => 'Events',
    'event' => 'Event',
    'posts' => 'Posts',
    'profiles' => 'Profiles',
    'pushes' => 'Notifications',
    'post' => 'Post',
    'push' => 'Notification',
    'reports' => 'Reports',
    'saved' => 'saved successfully',
    'updated' => 'updated successfully',
    'deleted' => 'deleted successfully',
    'not_found' => 'not found',
    'roles' => 'Roles',
    'role' => 'Role',
    'actions' => 'Actions',
    'action' => 'Action',
    'modules' => 'Modules',
    'module' => 'Module',
    'user' => 'User',
    'users' => 'Users',
    'alert' => 'Alert',
    'control_denied' => 'Control denied',
    'member' => 'Member',
    'members' => 'Members',
    'friends' => 'Friends',
    
];