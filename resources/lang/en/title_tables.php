<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Title',
    'subtitle' => 'SubTitle',
    'profile' => 'Profile',
    'category' => 'Category',
    'state' => 'State',
    'city' => 'City',
    'send_at' => 'Send at',
    'created_at' => 'Created em',
    'updated_at' => 'Updated em',
    'public' => 'Public',
    'description' => 'Description',
    'image' => 'Image',
    'legalfirstname' => 'Legal Firstname',
    'addressline2' => 'Address Line 2',
    'phonetype1' => 'Phone Type 1',
    'phonetype2' => 'Phone Type 2',
    'birthdate' => 'Birth Date',
    'othersmembers' => 'Others Members',
    'name' => 'Name',
    'email' => 'Email',
    'slug' => 'Slug',
    'role' => 'Role',
    'user' => 'User',
    'action' => 'Action',
    'module' => 'Module',
    'document' => 'Document',
    'profile' => 'Profile',
    
];
