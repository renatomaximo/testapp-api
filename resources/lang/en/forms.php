<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'created_at' => 'Created at',
    'updated_at' => 'Updated at',
    'save' => 'Save',
    'add_new' => 'Add New',
    'cancel' => 'Cancel',
    'back' => 'Back',
    'action' => 'Action',
    'are_you_sure' => 'Are you sure?',
];
