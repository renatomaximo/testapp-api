<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JD\Cloudder\Facades\Cloudder;

class ProductsController extends Controller
{

    /**
     * @SWG\Post(
     *      path="/products/getList",
     *      operationId="getList",
     *      tags={"Products"},
     *      summary="Get Products",
     *      description="Get Products",
     *      @SWG\Response(
     *          response=200,
     *          description="Return Data OK"
     *       ),
     *       @SWG\Response(
     *          response=403, 
     *          description="Products not found"
     *         ),
     *       @SWG\Response(
     *          response=401, 
     *          description="You are not allowed to access"
     *         )
     *     )
     *
     */
    public function getList() {
        try {
            $products = \App\Models\Product::where('active', true)->get();

            if ( count($products) > 0 ) {

                $data['result'] = $products;
                $data['error'] = false;
                $data['message'] = trans('api.200');

            } else {
                $data['result'] = 'Products not found';
                $data['error'] = true;
                $data['message'] = trans('api.403');  
            }
        } catch( Exception $e ){
            $data['result'] = $e->getMessage();
            $data['error'] = true;
            $data['message'] = trans('api.403');
        }
        return response()->json($data, 200);
    }



    /**
     * @SWG\Get(
     *      path="/products/get",
     *      operationId="get",
     *      tags={"Products"},
     *      summary="Get Product",
     *      description="Get Product",
     *      @SWG\Parameter(
     *          name="id",
     *          description="Product ID",
     *          required=true,
     *          type="integer",
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Return Data OK"
     *       ),
     *       @SWG\Response(
     *          response=403, 
     *          description="Product not found"
     *         ),
     *       @SWG\Response(
     *          response=401, 
     *          description="You are not allowed to access"
     *         )
     *     )
     *
     */
    public function get($id) {
        try {
            $product = \App\Models\Product::where('id','=',$id)->get();

            if ( count($product) > 0 ) {

                $data['result'] = $product;
                $data['error'] = false;
                $data['message'] = trans('api.200');

            } else {
                $data['result'] = 'Product not found';
                $data['error'] = true;
                $data['message'] = trans('api.403');  
            }
        } catch( Exception $e ){
            $data['result'] = $e->getMessage();
            $data['error'] = true;
            $data['message'] = trans('api.403');
        }
        return response()->json($data, 200);
    }


    /**
     * @SWG\Post(
     *      path="/products/create",
     *      operationId="get",
     *      tags={"Products"},
     *      summary="Create Product",
     *      description="Create Product",
     *      @SWG\Parameter(
     *          name="name",
     *          description="Title Product",
     *          required=true,
     *          type="string",
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="description",
     *          description="Description Product",
     *          required=false,
     *          type="string",
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="value",
     *          description="Value Product",
     *          required=true,
     *          type="string",
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="resale_value",
     *          description="Resale Value Product",
     *          required=false,
     *          type="string",
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="Image Product",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="active",
     *          description="Active",
     *          required=false,
     *          type="boolean",
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Return Data OK"
     *       ),
     *       @SWG\Response(
     *          response=403, 
     *          description="Product not found"
     *         ),
     *       @SWG\Response(
     *          response=401, 
     *          description="You are not allowed to access"
     *         )
     *     )
     *
     */
    public function create(Request $request) {
        try {

            if($request->active == true){
                $active = 1;
            } else {
                $active = 0;
            }

            $product = \App\Models\Product::create([
                'name' => $request->name,
                'description' => $request->description,
                'value' => $request->value,
                'resale_value' => $request->resale_value,
                'image' => $request->image,
                'active' => $active,
            ]);



            if ( !is_null($product) ) {

                if($request->hasFile('image')){
                    $image_name = $request->file('image')->getRealPath();
    
                    Cloudder::upload($image_name, null, array('folder' => '/testaap'), array());
    
                    $c = Cloudder::getResult();             
                    if($c){
                        $update = \App\Models\Product::where('id', $product->id)
                        ->update([
                            'image' => $c['url'],
                        ]);    
                    }
                }

                $data['result'] = $product;
                $data['error'] = false;
                $data['message'] = trans('api.200');

            } else {
                $data['result'] = 'Product not found';
                $data['error'] = true;
                $data['message'] = trans('api.403');  
            }
        } catch( Exception $e ){
            $data['result'] = $e->getMessage();
            $data['error'] = true;
            $data['message'] = trans('api.403');
        }
        return response()->json($data, 200);
    }



    /**
     * @SWG\Patch(
     *      path="/products/update",
     *      operationId="get",
     *      tags={"Products"},
     *      summary="Update Product",
     *      description="Update Product",
     *      @SWG\Parameter(
     *          name="id",
     *          description="Product ID",
     *          required=true,
     *          type="integer",
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="name",
     *          description="Title Product",
     *          required=true,
     *          type="string",
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="description",
     *          description="Description Product",
     *          required=false,
     *          type="string",
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="value",
     *          description="Value Product",
     *          required=true,
     *          type="string",
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="resale_value",
     *          description="Resale Value Product",
     *          required=false,
     *          type="string",
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="Image Product",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="active",
     *          description="Active",
     *          required=false,
     *          type="boolean",
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Return Data OK"
     *       ),
     *       @SWG\Response(
     *          response=403, 
     *          description="Product not found"
     *         ),
     *       @SWG\Response(
     *          response=401, 
     *          description="You are not allowed to access"
     *         )
     *     )
     *
     */
    public function update(Request $request) {
        try {

            $input = $request->all();

            if($request->active == true){
                $input['active'] = 1;
            } else {
                $input['active'] = 0;
            }

            $update = \App\Models\Product::where('id', $request->id)
            ->update($input);

            if($request->hasFile('image')){
                $image_name = $request->file('image')->getRealPath();

                Cloudder::upload($image_name, null, array('folder' => '/testaap'), array());

                $c = Cloudder::getResult();             
                if($c){
                    $update = \App\Models\Product::where('id', $request->id)
                    ->update([
                        'image' => $c['url'],
                    ]);    
                }
            }

            if ( $update == 1 ) {

                $data['result'] = 'Product updated successfully';
                $data['error'] = false;
                $data['message'] = trans('api.200');

            } else {
                $data['result'] = 'Product not found';
                $data['error'] = true;
                $data['message'] = trans('api.403');  
            }
        } catch( Exception $e ){
            $data['result'] = $e->getMessage();
            $data['error'] = true;
            $data['message'] = trans('api.403');
        }
        return response()->json($data, 200);
    }


    /**
     * @SWG\Delete(
     *      path="/products/delete",
     *      operationId="delete",
     *      tags={"Products"},
     *      summary="Delete Product",
     *      description="Delete Product",
     *      @SWG\Parameter(
     *          name="id",
     *          description="Product ID",
     *          required=true,
     *          type="integer",
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Return Data OK"
     *       ),
     *       @SWG\Response(
     *          response=403, 
     *          description="Product not found"
     *         ),
     *       @SWG\Response(
     *          response=401, 
     *          description="You are not allowed to access"
     *         )
     *     )
     *
     */
    public function delete($id) {
        try {
            $product = \App\Models\Product::where('id','=',$id)->delete();

            if ( $product == 1) {

                $data['result'] = 'Product deleted successfully';
                $data['error'] = false;
                $data['message'] = trans('api.200');

            } else {
                $data['result'] = 'Product not found';
                $data['error'] = true;
                $data['message'] = trans('api.403');  
            }
        } catch( Exception $e ){
            $data['result'] = $e->getMessage();
            $data['error'] = true;
            $data['message'] = trans('api.403');
        }
        return response()->json($data, 200);
    }
}
