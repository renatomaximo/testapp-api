<?php

namespace App\Http\Controllers;

use JD\Cloudder\Facades\Cloudder;
use Illuminate\Http\Request;

class ImageUploadController extends Controller
{
    public function uploadImages($image, $name = 'image'){

       $image_name = $image->file($name)->getRealPath();

       Cloudder::upload($image_name, null, array(), array());

       $c = Cloudder::getResult();             
       if($c){
          return $c;
       }

   }
}
