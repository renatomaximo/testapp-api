<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public $fillable = [
        'name',
        'description',
        'value',
        'resale_value',
        'image',
        'active',
    ];
}
