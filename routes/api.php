<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group([

    'middleware' => 'api',
    'prefix' => 'v1'

], function ($router) {
    //products
    Route::post('products/getList', 'ProductsController@getList');
    Route::get('products/get/{id}', 'ProductsController@get');
    Route::post('products/create', 'ProductsController@create');
    Route::patch('products/update', 'ProductsController@update');
    Route::delete('products/delete/{id}', 'ProductsController@delete');
    
});

